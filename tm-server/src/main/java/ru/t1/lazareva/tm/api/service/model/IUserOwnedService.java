package ru.t1.lazareva.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

}
package ru.t1.lazareva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.dto.ISessionDtoService;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto, ISessionDtoRepository> implements ISessionDtoService {

    public SessionDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}
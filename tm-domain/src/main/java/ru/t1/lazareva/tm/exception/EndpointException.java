package ru.t1.lazareva.tm.exception;

import org.jetbrains.annotations.NotNull;

public final class EndpointException extends AbstractException {

    public EndpointException(@NotNull String message) {
        super(message);
    }

}